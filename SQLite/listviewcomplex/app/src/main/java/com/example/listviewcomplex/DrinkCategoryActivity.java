package com.example.listviewcomplex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.example.listviewsimplified.R;

import java.util.ArrayList;

public class DrinkCategoryActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_category);

        // 1. get the ListView
        ListView drinkList = (ListView) findViewById(R.id.item_list);

        SQLiteOpenHelper starbuzzDBHelper = new StarbuzzDatabaseHelper(this);
        try{
            db = starbuzzDBHelper.getReadableDatabase();
            cursor = db.query(
                    "DRINK",
                    new String[] {"_id","NAME"},
                    null,null,null,null,null
            );

            SimpleCursorAdapter listAdapter = new SimpleCursorAdapter(
                    this,
                    android.R.layout.simple_list_item_1,
                    cursor,
                    new String[]{"NAME"},
                    new int[]{android.R.id.text1},
                    0
            );
            drinkList.setAdapter(listAdapter);
        }catch (SQLiteException e){
            Toast toast = Toast.makeText(this,"Database not available",
                    Toast.LENGTH_SHORT);
            toast.show();
        }

        // 2. get data via ArrayAdapter
//        DrinkCategoryListAdapter dynamicLayoutAdapter = new DrinkCategoryListAdapter(this
//                , Drink.drinks);
//        ArrayAdapter<Drink> dynamicLayoutAdapter = new ArrayAdapter<>(
//                this,
//                android.R.layout.simple_list_item_1,
//                Drink.drinks
//        );
        // 3. attach ArrayAdapter to ListView
//        drinkList.setAdapter(dynamicLayoutAdapter);
        // 4. set up AdapterView OnItemClickListener
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
                Intent intent = new Intent(DrinkCategoryActivity.this
                        , DrinkActivity.class);
                intent.putExtra(DrinkActivity.DRINK_ID,(int)id);
                startActivity(intent);
            }
        };
        // 5. attach listener to ListView
        drinkList.setOnItemClickListener(itemClickListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cursor.close();
        db.close();
    }
}
