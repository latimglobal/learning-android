package com.example.listviewcomplex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.example.listviewsimplified.R;

public class TopLevelActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private Cursor favCursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_level);

        setupOptionsListView();

        setupFavoritesListView();

    }



    private void setupOptionsListView() {
        // 1. get the ListView
        ListView categoryList = (ListView) findViewById(R.id.category_list);
        // 2. setup OnItemClickListener
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
                if (position == 0){
                    Intent intent = new Intent(TopLevelActivity.this, DrinkCategoryActivity.class);
                    startActivity(intent);
                }
            }
        };
        // 3. attach listener to ListView
        categoryList.setOnItemClickListener(itemClickListener);
    }
    private void setupFavoritesListView() {
        ListView favList = (ListView) findViewById(R.id.list_favorites);


        try{
            SQLiteOpenHelper starbuzzDBHelper = new StarbuzzDatabaseHelper(this);
            db = starbuzzDBHelper.getReadableDatabase();
            favCursor = db.query(
                    "DRINK",
                    new String[] {"_id","NAME"},
                    "FAVORITE = 1",
                    null,null,null,null
            );

            CursorAdapter listAdapter = new SimpleCursorAdapter(
                    TopLevelActivity.this,
                    android.R.layout.simple_list_item_1,
                    favCursor,
                    new String[]{"NAME"},
                    new int[]{android.R.id.text1},
                    0
            );
            favList.setAdapter(listAdapter);
        }catch (SQLiteException e){
            Toast toast = Toast.makeText(this,"Database not available",
                    Toast.LENGTH_SHORT);
            toast.show();
        }

        favList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View v, int position, long id) {
                Intent intent = new Intent(TopLevelActivity.this
                        , DrinkActivity.class);
                intent.putExtra(DrinkActivity.DRINK_ID,(int)id);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        favCursor.close();
        db.close();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Cursor newCursor = db.query("DRINK",
                new String[]{"_id","NAME"},
                "FAVORITE = 1",
                null,null,null,null);
        ListView favList = (ListView) findViewById(R.id.list_favorites);
        CursorAdapter adapter = (CursorAdapter) favList.getAdapter();
        adapter.changeCursor(newCursor);
        favCursor = newCursor;
    }
}
