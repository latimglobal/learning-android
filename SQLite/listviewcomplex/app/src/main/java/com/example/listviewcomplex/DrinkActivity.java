package com.example.listviewcomplex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.listviewsimplified.R;

import java.util.ArrayList;

public class DrinkActivity extends AppCompatActivity {

    public static final String DRINK_ID = "drinkId";


    public void onFavoriteClicked(View view) {
        int drinkId = (Integer) getIntent().getExtras().get(DrinkActivity.DRINK_ID);
        CheckBox favChk = (CheckBox) findViewById(R.id.favorite);
        ContentValues contentValues = new ContentValues();
        contentValues.put("FAVORITE", favChk.isChecked());

        SQLiteOpenHelper starbuzzHelper = new StarbuzzDatabaseHelper(this);
        try{
            SQLiteDatabase db = starbuzzHelper.getWritableDatabase();
            db.update(
                    "DRINK",
                    contentValues,
                    "_id=?",
                    new String[]{Integer.toString(drinkId)}
            );
            db.close();
        }catch (SQLiteException e){
            Toast toast = Toast.makeText(this,"Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        ArrayList<Drink> drinks = new ArrayList<Drink>();
        drinks.add(new Drink("Latte","A quick mix of chocolate", R.drawable.latte));
        drinks.add(new Drink("Capuccino","A nice mix of different flavors",R.drawable.cappuccino));
        drinks.add(new Drink("Filter","A filter of everything",R.drawable.filter));

        // 1. get drinkId from intent
        int drinkId = (Integer) getIntent().getExtras().get(DrinkActivity.DRINK_ID);
        // 2. get appropriate drink from the drink list
//        Drink currentDrink = Drink.drinks[drinkId];
        // 3. attach drink photo resource
        SQLiteOpenHelper starbuzzDBHelper = new StarbuzzDatabaseHelper(this);
        try{
            SQLiteDatabase db = starbuzzDBHelper.getReadableDatabase();
            Cursor cursor = db.query(
                "DRINK",
                    new String[] {"NAME","DESCRIPTION","IMAGE_RESOURCE_ID","FAVORITE"},
                    "_id=?",
                    new String[]{Integer.toString(drinkId)},
                    null,null,null
            );
            if (cursor.moveToFirst()){
                String nameText = cursor.getString(0);
                String descText = cursor.getString(1);
                int photoId = cursor.getInt(2);
                boolean isFavorite = (cursor.getInt(3) == 1);

                ImageView photo = (ImageView) findViewById(R.id.photo);
                photo.setImageResource(photoId);
                photo.setContentDescription(nameText);
                // 4. attach drink name
                TextView name = (TextView) findViewById(R.id.name);
                name.setText(nameText);
                // 5. attach drink description
                TextView desc = (TextView) findViewById(R.id.desc);
                desc.setText(descText);

                CheckBox favCheckbox = (CheckBox) findViewById(R.id.favorite);
                favCheckbox.setChecked(isFavorite);
            }

            cursor.close();
            db.close();
        } catch (SQLiteException e){
            Toast toast = Toast.makeText(this,"Database unavailable",Toast.LENGTH_SHORT);
            toast.show();
        }


    }
}
