package com.example.listviewcomplex;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.listviewsimplified.R;

public class StarbuzzDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "starbuzz";
    private static final int DB_VERSION = 2;

    StarbuzzDatabaseHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db,0,DB_VERSION);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
//        if (oldVersion <1){

            db.execSQL("CREATE TABLE DRINK (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "NAME TEXT," +
                    "DESCRIPTION TEXT," +
                    "IMAGE_RESOURCE_ID INTEGER," +
                    "FAVORITE INTEGER);");
            insertDrink(db,"Latte", "Espresso and steamed milk", R.drawable.latte,0);
            insertDrink(db,"Cappuccino", "Espresso, hot milk and steamed-milk foam", R.drawable.cappuccino,0);
            insertDrink(db,"Filter", "Our best dripped coffee", R.drawable.filter,0);
//        }
//        if (oldVersion < 2){
////            updateDrink(db);
////            db.execSQL("ALTER TABLE DRINK ADD COLUMN FAVORITE NUMERIC");
//        }
//        if (oldVersion < 3){
//            deleteDrink(db);
//        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion,newVersion);
    }

    private static void insertDrink(SQLiteDatabase db, String name, String description, int resourceId,int favorite){
        ContentValues drinkValues = new ContentValues();
        drinkValues.put("NAME", name);
        drinkValues.put("DESCRIPTION", description);
        drinkValues.put("IMAGE_RESOURCE_ID", resourceId);
        drinkValues.put("FAVORITE",favorite);
        db.insert("DRINK",null,drinkValues);
    }
    private static void updateDrink(SQLiteDatabase db){
        ContentValues drinkValues = new ContentValues();
        drinkValues.put("DESCRIPTION", "Tasty");
        db.update("DRINK",drinkValues, "NAME=? OR DESCRIPTION=?"
                , new String[]{"Latte","Our best dripp coffee"});
    }
    private static void deleteDrink(SQLiteDatabase db){
        db.delete("DRINK", "NAME=?", new String[]{"Latte"});
    }
}
