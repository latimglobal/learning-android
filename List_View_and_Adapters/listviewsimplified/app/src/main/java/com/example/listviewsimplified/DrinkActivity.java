package com.example.listviewsimplified;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DrinkActivity extends AppCompatActivity {

    public static final String DRINK_ID = "drinkId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        // 1. get drinkId from intent
        int drinkId = (Integer) getIntent().getExtras().get(DrinkActivity.DRINK_ID);
        // 2. get appropriate drink from the drink list
        Drink currentDrink = Drink.drinks[drinkId];
        // 3. attach drink photo resource
        ImageView photo = (ImageView) findViewById(R.id.photo);
        photo.setImageResource(currentDrink.getImageResourceId());
        photo.setContentDescription(currentDrink.getName());
        // 4. attach drink name
        TextView name = (TextView) findViewById(R.id.name);
        name.setText(currentDrink.getName());
        // 5. attach drink description
        TextView desc = (TextView) findViewById(R.id.desc);
        desc.setText(currentDrink.getDesc());

    }
}
