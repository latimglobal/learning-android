package com.example.listviewsimplified;

import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DrinkCategoryListAdapter extends ArrayAdapter<Drink> {

    public DrinkCategoryListAdapter(@NonNull Activity context, ArrayList<Drink> drinks) {
        super(context, 0, drinks);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // 1. get view in a View object
        View currentListItemView = convertView;
        // 2. inflate layout if view is null
        if (currentListItemView == null){
            LayoutInflater.from(getContext()).inflate(R.layout.drink_list,parent,false);
        }
        // 3. get current Drink item from the position
        Drink currentDrink = getItem(position);
        // 4. set resource to View photo elem
        ImageView photo = (ImageView) currentListItemView.findViewById(R.id.photo);
        photo.setImageResource(currentDrink.getImageResourceId());
        // 5. set name to View name elem
        TextView name = (TextView) currentListItemView.findViewById(R.id.name);
        name.setText(currentDrink.getName());
        // 5. return View
        return currentListItemView;
    }
}
