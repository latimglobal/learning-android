package com.example.listviewcomplex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.listviewsimplified.R;

import java.util.ArrayList;

public class DrinkCategoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_category);

        // 1. get the ListView
        ListView drinkList = (ListView) findViewById(R.id.item_list);

        // 2. get data via ArrayAdapter
        DrinkCategoryListAdapter dynamicLayoutAdapter = new DrinkCategoryListAdapter(this, Drink.drinks);
        // 3. attach ArrayAdapter to ListView
        drinkList.setAdapter(dynamicLayoutAdapter);
        // 4. set up AdapterView OnItemClickListener
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view, int position, long id) {
                Intent intent = new Intent(DrinkCategoryActivity.this, DrinkActivity.class);
                intent.putExtra(DrinkActivity.DRINK_ID,(int)id);
                startActivity(intent);
            }
        };
        // 5. attach listener to ListView
        drinkList.setOnItemClickListener(itemClickListener);
    }
}
