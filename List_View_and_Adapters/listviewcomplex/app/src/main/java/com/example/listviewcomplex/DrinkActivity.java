package com.example.listviewcomplex;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.listviewsimplified.R;

import java.util.ArrayList;

public class DrinkActivity extends AppCompatActivity {

    public static final String DRINK_ID = "drinkId";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        ArrayList<Drink> drinks = new ArrayList<Drink>();
        drinks.add(new Drink("Latte","A quick mix of chocolate", R.drawable.latte));
        drinks.add(new Drink("Capuccino","A nice mix of different flavors",R.drawable.cappuccino));
        drinks.add(new Drink("Filter","A filter of everything",R.drawable.filter));

        // 1. get drinkId from intent
        int drinkId = (Integer) getIntent().getExtras().get(DrinkActivity.DRINK_ID);
        // 2. get appropriate drink from the drink list
        Drink currentDrink = Drink.drinks[drinkId];
        // 3. attach drink photo resource
        ImageView photo = (ImageView) findViewById(R.id.photo);
        photo.setImageResource(currentDrink.getImageResourceId());
        photo.setContentDescription(currentDrink.getName());
        // 4. attach drink name
        TextView name = (TextView) findViewById(R.id.name);
        name.setText(currentDrink.getName());
        // 5. attach drink description
        TextView desc = (TextView) findViewById(R.id.desc);
        desc.setText(currentDrink.getDesc());

    }
}
