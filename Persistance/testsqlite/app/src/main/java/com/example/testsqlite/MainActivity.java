package com.example.testsqlite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static com.example.testsqlite.UserDatabase.MIGRATION_1_2;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserDatabase userDB = Room.databaseBuilder(getApplicationContext(),
                UserDatabase.class, "users")
                .allowMainThreadQueries()
                //.fallbackToDestructiveMigration()
                .addMigrations(MIGRATION_1_2)
                .build();

        // db calls
        UserDao userDao = userDB.userDao();
//        userDao.deleteAll();

        List<User> userData = new ArrayList<>();
        userData.add(new User("Tanvir", "Ahmed Chowdhury"));
        userData.add(new User("Rezaul", "Karim"));

        userDao.insertAll(userData);

//        for (User u: userData){
//            userDao.insert(u);
//        }

        List<User> users = userDao.getAll();

        for (int i=0;i<users.size();i++){
            Log.v("USER "+i, users.get(i).toString());
        }
    }
}
