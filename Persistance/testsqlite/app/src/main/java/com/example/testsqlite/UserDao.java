package com.example.testsqlite;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao {
    @Query("SELECT *,rowid FROM users")
    List<User> getAll();

    @Query("SELECT *,rowid FROM users WHERE rowid IN (:userIds)")
    List<User> loadAllByIds(int[] userIds);

    @Query("SELECT *,rowid FROM users WHERE first_name LIKE :first AND " +
            "last_name LIKE :last LIMIT 1")
    User findByName(String first, String last);

    @Query("DELETE FROM users WHERE 1=1")
    void deleteAll();

    @Insert
    void insert(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<User> users);

    @Insert
    void insertFirstName(User user);

    @Insert
    void insertLastName(User user);

    @Delete
    void delete(User user);
}