package com.example.listviewcomplex;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.listviewsimplified.R;

import java.util.ArrayList;

public class DrinkActivity extends AppCompatActivity {

    public static final String DRINK_ID = "drinkId";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink);

        ArrayList<Drink> drinks = new ArrayList<Drink>();
        drinks.add(new Drink("Latte","A quick mix of chocolate", R.drawable.latte));
        drinks.add(new Drink("Capuccino","A nice mix of different flavors",R.drawable.cappuccino));
        drinks.add(new Drink("Filter","A filter of everything",R.drawable.filter));

        // 1. get drinkId from intent
        int drinkId = (Integer) getIntent().getExtras().get(DrinkActivity.DRINK_ID);
        // 2. get appropriate drink from the drink list
//        Drink currentDrink = Drink.drinks[drinkId];
        // 3. attach drink photo resource
        SQLiteOpenHelper starbuzzDBHelper = new StarbuzzDatabaseHelper(this);
        try{
            SQLiteDatabase db = starbuzzDBHelper.getReadableDatabase();
            Cursor cursor = db.query(
                "DRINK",
                    new String[] {"NAME","DESCRIPTION","IMAGE_RESOURCE_ID","FAVORITE"},
                    "_id=?",
                    new String[]{Integer.toString(drinkId)},
                    null,null,null
            );
            if (cursor.moveToFirst()){
                String nameText = cursor.getString(0);
                String descText = cursor.getString(1);
                int photoId = cursor.getInt(2);
                boolean isFavorite = (cursor.getInt(3) == 1);

                ImageView photo = (ImageView) findViewById(R.id.photo);
                photo.setImageResource(photoId);
                photo.setContentDescription(nameText);
                // 4. attach drink name
                TextView name = (TextView) findViewById(R.id.name);
                name.setText(nameText);
                // 5. attach drink description
                TextView desc = (TextView) findViewById(R.id.desc);
                desc.setText(descText);

                CheckBox favCheckbox = (CheckBox) findViewById(R.id.favorite);
                favCheckbox.setChecked(isFavorite);
            }

            cursor.close();
            db.close();
        } catch (SQLiteException e){
            Toast toast = Toast.makeText(this,"Database unavailable",Toast.LENGTH_SHORT);
            toast.show();
        }


    }

    public void onFavoriteClicked(View view) {
        int drinkId = (Integer) getIntent().getExtras().get(DrinkActivity.DRINK_ID);

        new UpdateDrinkTask().execute(drinkId);
    }

    private class UpdateDrinkTask extends AsyncTask<Integer,Void,Boolean>{

        private ContentValues drinkValues;

        protected void onPreExecute(){
            CheckBox favChk = (CheckBox) findViewById(R.id.favorite);
            drinkValues = new ContentValues();
            drinkValues.put("FAVORITE", favChk.isChecked());
        }

        @Override
        protected Boolean doInBackground(Integer... drinks) {
            int drinkId = drinks[0];
            SQLiteOpenHelper starbuzzHelper = new StarbuzzDatabaseHelper(DrinkActivity.this);
            try{
                SQLiteDatabase db = starbuzzHelper.getWritableDatabase();
                db.update(
                        "DRINK",
                        drinkValues,
                        "_id=?",
                        new String[]{Integer.toString(drinkId)}
                );
                db.close();
                return true;
            }catch (SQLiteException e){
                return false;

            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (!success){
                Toast toast = Toast.makeText(DrinkActivity.this,"Database unavailable", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
}
