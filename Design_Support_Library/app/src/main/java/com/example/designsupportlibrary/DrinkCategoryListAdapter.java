package com.example.designsupportlibrary;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

class DrinkCategoryListAdapter extends ArrayAdapter<Drink> {
    public DrinkCategoryListAdapter(Activity context, Drink[] drinks) {
        super(context, 0, drinks);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // 1. get current View
        View currentListItem = convertView;
        // 2. inflate layout if current view null
        if (currentListItem == null) {
            currentListItem = LayoutInflater.from(getContext()).inflate(R.layout.drink_list, parent, false);
        }
        // 3. get current Drink object
        Drink drink = getItem(position);
        // 4. set photo resource
        ImageView photo = (ImageView) currentListItem.findViewById(R.id.photo);
        photo.setImageResource(drink.getImageResourceId());
        // 5. set name text
        TextView name = (TextView) currentListItem.findViewById(R.id.name);
        name.setText(drink.getName());
        // 6. return current View
        return currentListItem;
    }
}
