package com.example.designsupportlibrary;

public class Drink {
    private String name;
    private String desc;
    private int imageResourceId;

    public Drink(String name, String desc, int imageResourceId) {
        this.name = name;
        this.desc = desc;
        this.imageResourceId = imageResourceId;
    }

    public static final Drink[] drinks = {
            new Drink("Latte","A quick mix of chocolate", R.drawable.latte),
            new Drink("Capuccino","A nice mix of different flavors",R.drawable.cappuccino),
            new Drink("Filter","A filter of everything",R.drawable.filter)
    };

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
