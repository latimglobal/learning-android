package bd.com.basicfragments.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import bd.com.basicfragments.R;
import bd.com.basicfragments.models.Workout;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutDetailFragment extends Fragment {

    private int workout_id;

    public void setWorkout_id(int workout_id) {
        this.workout_id = workout_id;
    }

    @Override
    public void onStart() {
        super.onStart();
        View view = getView();
        TextView title = view.findViewById(R.id.title);
        TextView desc = view.findViewById(R.id.desc);

        Workout wk = Workout.workouts[workout_id];
        title.setText(wk.getTitle());
        desc.setText(wk.getDesc());
    }
//    public WorkoutDetailFragment() {
//        // Required empty public constructor but can be excluded if there
//        // are no other constructors as java code generates one by default
//    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            this.workout_id = (int) savedInstanceState.getLong("workoutId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workout_detail, container, false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong("workoutId",workout_id);
    }
}
