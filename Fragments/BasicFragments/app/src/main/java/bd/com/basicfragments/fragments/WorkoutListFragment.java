package bd.com.basicfragments.fragments;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import bd.com.basicfragments.R;
import bd.com.basicfragments.models.Workout;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutListFragment extends ListFragment {


    public static interface Listener{
        void setItemId(long id);
    }

    private Listener listener;

//    public WorkoutListFragment() {
//        // Required empty public constructor
//    }
//

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        String[] names = new String[Workout.workouts.length];
        for (int i=0;i<names.length;i++){
            names[i] = Workout.workouts[i].getTitle();
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                inflater.getContext(),
                android.R.layout.simple_list_item_1,
                names
        );

        setListAdapter(arrayAdapter);

        return super.onCreateView(inflater,container,savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (Listener) context;
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        listener.setItemId(id);
    }
}
