package bd.com.dynamicfragments.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import bd.com.dynamicfragments.R;
import bd.com.dynamicfragments.fragments.WorkoutDetailFragment;

public class DetailActivity extends AppCompatActivity {
    public static final String EXTRA_WORKOUT_ID = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        WorkoutDetailFragment frag = (WorkoutDetailFragment) getSupportFragmentManager().findFragmentById(R.id.detail_frag);
        frag.setWorkout_id((int) getIntent().getExtras().get(EXTRA_WORKOUT_ID));
    }
}
