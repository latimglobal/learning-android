package bd.com.dynamicfragments.models;

public class Workout {
    private String title;
    private String desc;

    public Workout(String title, String desc) {
        this.title = title;
        this.desc = desc;
    }

    public static final Workout[] workouts = {
            new Workout("w1", "desc1"),
            new Workout("w2","desc2")
    };

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }

    @Override
    public String toString() {
        return "Workout{" +
                "name='" + title + '\'' +
                '}';
    }
}
