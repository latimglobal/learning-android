package com.example.designsupportlibrary;

public class Pizza {
    private String name;
    private int imageResourceId;

    public static final Pizza[] pizzas = {
        new Pizza("Diavolo", R.drawable.diavolo),
        new Pizza("Funghi", R.drawable.funghi)
    };

    private Pizza(String name, int imageId){
        this.name = name;
        this.imageResourceId = imageId;
    }

    public String getName() {
        return name;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }
}
